//
//  ListaPokemonsController.swift
//  pokedex
//
//  Created by COTEMIG on 24/02/22.
//

import UIKit

class ListaPokemonsController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var pokemons: [pokemon] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Minhacelula") as? TableViewCell {
            cell.nome.text = pokemons[indexPath.row].nome
            cell.tipo_1.text = pokemons[indexPath.row].tipo_1
            cell.tipo_2.text = pokemons[indexPath.row].tipo_2
            cell.bulbassauro.image = UIImage(named: pokemons[indexPath.row].img)
            cell.contentView.backgroundColor = hexStringToUIColor(hex: pokemons[indexPath.row].color)
            
            if pokemons[indexPath.row].tipo_1 == "Fogo"{
                cell.botao1.backgroundColor = UIColor(named: "FundoTrasparente")
                if pokemons[indexPath.row].tipo_2 != "" {
                    cell.botoao2.backgroundColor = UIColor(named: "FundoTrasparente")
                }else{
                    cell.botoao2.backgroundColor = UIColor(named: "Color")
                }
            }else if pokemons[indexPath.row].tipo_1 == "Água"  {
                cell.botao1.backgroundColor = UIColor(named: "FundoTrasparente")
                if pokemons[indexPath.row].tipo_2 != "" {
                    cell.botoao2.backgroundColor = UIColor(named: "FundoTrasparente")
                }else{
                    cell.botoao2.backgroundColor = UIColor(named: "a")
                }
            }else if pokemons[indexPath.row].tipo_1 == "Grama" || pokemons[indexPath.row].tipo_2 == "Veneno" {
                cell.botao1.backgroundColor = UIColor(named: "FundoTrasparente")
                if pokemons[indexPath.row].tipo_2 != "" {
                    cell.botoao2.backgroundColor = UIColor(named: "FundoTrasparente")
                }else{
                    cell.botoao2.backgroundColor = UIColor(named: "FundoVerde")
                }
            }else if pokemons[indexPath.row].tipo_1 == "Elétrico" {
                cell.botao1.backgroundColor = UIColor(named: "FundoTrasparente")
                if pokemons[indexPath.row].tipo_2 != "" {
                    cell.botoao2.backgroundColor = UIColor(named: "FundoTrasparente")
                }else{
                    cell.botoao2.backgroundColor = UIColor(named: "amarelo")
                }
            }
            return cell
        }else{
            fatalError("Deu erro")
        }
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        pokemons = [
            pokemon(nome: "Bulbassauro", tipo_1: "Grama", tipo_2: "Veneno", img: "ivvysauro",color:"FundoVerde"),
            pokemon(nome: "Ivyssauro", tipo_1: "Grama", tipo_2: "Veneno", img: "ivy2", color: "FundoVerde"),
            pokemon(nome: "Venussauro", tipo_1: "Grama", tipo_2: "Veneno", img: "venussauro",color:"FundoVerde"),
            pokemon(nome: "Charmander", tipo_1: "Fogo", tipo_2: "", img: "charmander", color: "Color" ),
            pokemon(nome: "Charmeleon", tipo_1: "Fogo", tipo_2: "", img: "charmeleon", color: "Color"),
            pokemon(nome: "Charizard", tipo_1: "Fogo", tipo_2: "Voador", img: "charizard", color: "Color"),
            pokemon(nome: "Squirtle", tipo_1: "Água", tipo_2: "", img: "squirtle", color: "a"),
            pokemon(nome: "Wartortle", tipo_1: "Água", tipo_2: "", img: "wartortle", color: "a"),
            pokemon(nome: "Blastoise", tipo_1: "Água", tipo_2: "", img: "blastoise", color: "a"),
            pokemon(nome: "Pikachu", tipo_1: "Elétrico", tipo_2: "", img: "pikachu", color: "amarelo"),
        ]
    }
    
    func hexStringToUIColor(hex:String) -> UIColor {
        return UIColor(named: hex) ?? .darkGray
    }
    struct pokemon {
        var nome: String
        var tipo_1: String
        var tipo_2: String
        var img: String
        var color: String
    }

}

